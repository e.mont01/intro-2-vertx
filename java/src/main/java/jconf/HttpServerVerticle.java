package jconf;

import io.vertx.core.Vertx;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HttpServerVerticle extends AbstractVerticle
{
  @Override
  public void start(Future<Void> future)
  {
    log.info("Welcome to Vertx");
    vertx.createHttpServer()
        .requestHandler(r -> r.response().end("Bienvenido al PanamaJUG 2020.12.05"))
        .listen(config().getInteger("http.port", 9090),
          result -> {
            if (result.succeeded()) {
                future.complete();
            } else {
                future.fail(result.cause());
            }
        });
  }
}
