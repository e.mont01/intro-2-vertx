package jconf;

import io.vertx.core.Launcher;

import io.vertx.core.Future;
import io.vertx.reactivex.core.AbstractVerticle;
import jconf.services.TasksVerticle;

public class Main
{

  public static void main(String[] args)
  {
    Launcher.executeCommand("run", LauncherVerticle.class.getName());
  }

  public static class LauncherVerticle extends AbstractVerticle
  {
    @Override
    public void start(Future<Void> startFuture)
    {
      vertx.rxDeployVerticle(TasksVerticle.class.getName())
        .toCompletable()
        .andThen(vertx.rxDeployVerticle(EventBusVerticle.class.getName()))
        .toCompletable()
        .subscribe(startFuture::complete, startFuture::fail);
    }

  }

}
