package jconf;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
@JsonSerialize
@JsonDeserialize(builder = Task.TaskBuilder.class)
public class Task
{
  @JsonProperty
  int id;
  @JsonProperty
  String description;
  @JsonProperty
  String priority;
}
