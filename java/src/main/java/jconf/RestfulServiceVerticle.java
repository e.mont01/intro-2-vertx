package jconf;

import lombok.extern.slf4j.Slf4j;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

@Slf4j
public class RestfulServiceVerticle extends AbstractVerticle
{
  @Override
  public void start(Future<Void> future)
  {
    Router router = Router.router(vertx);
    router.get("/api/v1/jconf/tasks/:id")
      .handler(this::getTasks);

    vertx.createHttpServer()
      .requestHandler(router::accept)
      .listen(config().getInteger("http.port", 9090), result -> {
        if (result.succeeded()) {
          future.complete();
        } else {
          future.fail(result.cause());
        }
      });
  }

  private void getTasks(RoutingContext ctx)
  {
    String id = ctx.request().getParam("id");
    Task task = Task.builder()
      .id(Integer.parseInt(id))
      .description("hello world from Panama: " + id)
      .priority("low")
      .build();

    ctx.response()
      .putHeader("Content-Type", "application/json")
      .setStatusCode(200)
      .end(Json.encodePrettily(task));
  }
}
