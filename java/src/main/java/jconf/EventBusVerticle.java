package jconf;

import io.reactivex.Single;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;

import io.reactivex.Single;
import io.vertx.core.Future;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.eventbus.Message;
import jconf.services.TasksVerticle;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;

import static jconf.services.TasksVerticle.TASK_SERVICE_ADDRESS;

@Slf4j
public class EventBusVerticle extends AbstractVerticle
{

  @Override
  public void start(Future<Void> startFuture)
  {
    vertx.deployVerticle(new TasksVerticle());
    JsonObject message = new JsonObject()
      .put("id", 0)
      .put("description", "Hello world")
      .put("priority", "high");
    log.info("Creating task: {}", message.toString());

    vertx.eventBus().<JsonObject>rxRequest(TASK_SERVICE_ADDRESS, message, action("save"))
      .map(Message::body)
      .doOnSuccess(data -> log.info("Task created: {}", data.toString()))
      .map(data -> new JsonObject().put("id", data.getInteger("id")))
      .flatMap(id -> vertx.eventBus().<JsonObject>rxRequest(TASK_SERVICE_ADDRESS, id, action("find")))
      .map(Message::body)
      .doOnSuccess(task -> log.info("Task {}", task))
      .ignoreElement()
      .subscribe(startFuture::complete, startFuture::fail);
  }

  private static DeliveryOptions action(String action)
  {
    return new DeliveryOptions().addHeader("action", action);
  }
}
