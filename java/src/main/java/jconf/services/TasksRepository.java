package jconf.services;

import io.reactivex.Maybe;
import io.reactivex.Single;
import jconf.Task;

public interface TasksRepository
{
  Single<Task> save(Task task);
  Maybe<Task> findById(int id);
}
