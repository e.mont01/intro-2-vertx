package jconf.services;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.vertx.core.json.JsonArray;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import jconf.Task;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
public class MariadbTaskRepository implements TasksRepository
{

  JDBCClient client;

  @Override
  public Single<Task> save(Task task)
  {
    final var args = (new JsonArray())
      .add(task.getDescription())
      .add(task.getPriority());
    final String sql = "INSERT INTO tasks (description, priority) VALUES(?, ?)";

    return client.rxUpdateWithParams(sql, args)
      .map(rs -> task.toBuilder()
        .id(rs.getKeys().getInteger(0))
        .build()
      );
  }

  @Override
  public Maybe<Task> findById(int id)
  {
    final var args = (new JsonArray()).add(id);
    final String sql = "SELECT id, description, priority FROM tasks WHERE id = ?";

    return client.rxQuerySingleWithParams(sql, args)
      .map(rs -> Task.builder()
        .id(rs.getInteger(0))
        .description(rs.getString(1))
        .priority(rs.getString(2))
        .build()
      );
  }

}
