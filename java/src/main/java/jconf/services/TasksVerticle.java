package jconf.services;

import io.reactivex.Single;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.eventbus.MessageConsumer;
import io.vertx.reactivex.ext.jdbc.JDBCClient;
import jconf.Task;

public class TasksVerticle extends AbstractVerticle
{

  public static final String TASK_SERVICE_ADDRESS = "task.service";

  private TasksRepository repository;
  private MessageConsumer<JsonObject> consumer;

  @Override
  public void start(Future<Void> startFuture)
  {
    JsonObject config = new JsonObject()
      .put("url", "jdbc:mariadb://localhost:3306/panamajug")
      .put("user", "panamajug")
      .put("password", "iddaksyieddEur7")
      .put("max_pool_size", 5);
    var client = JDBCClient.createShared(vertx, config, "datasource");

    repository = MariadbTaskRepository.builder()
      .client(client)
      .build();

    consumer = vertx.eventBus().<JsonObject>consumer(TASK_SERVICE_ADDRESS, message -> {
      switch (message.headers().get("action")) {
        case "save":
          repository
            .save(message.body().mapTo(Task.class))
            .map(JsonObject::mapFrom)
            .subscribe(message::reply, e -> message.fail(500, e.getMessage()));
          break;
        case "find":
          repository.findById(message.body().getInteger("id"))
            .switchIfEmpty(Single.error(new RuntimeException("Task not found")))
            .map(JsonObject::mapFrom)
            .subscribe(message::reply, e -> message.fail(404, e.getMessage()));
          break;
        default:
          message.fail(400, "Unknown action.");
      }
    });

    consumer.completionHandler(startFuture);
  }

  @Override
  public void stop(Future<Void> stopFuture)
  {
    consumer.unregister(stopFuture);
  }
}
