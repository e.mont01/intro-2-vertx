package jconf;

import io.vertx.core.Vertx;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HelloVerticle extends AbstractVerticle
{
  @Override
  public void start(Future<Void> future)
  {
    log.info("Welcome to Vertx");
  }
}
