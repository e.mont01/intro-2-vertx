package jconf;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest
{
  @Test
  void toJson()
  {
    final String expected = "{\"id\":1,\"description\":\"hello\",\"priority\":\"high\"}";
    final String actual = Json.encode(Task.builder()
      .id(1)
      .description("hello")
      .priority("high")
      .build()
    );

    assertEquals(expected, actual);
  }

  @Test
  void fromJson()
  {
    final Task expected = Task.builder()
      .id(100)
      .description("test")
      .priority("low")
      .build();

    final Task actual = Json.decodeValue(
      "{\"id\":100,\"description\":\"test\",\"priority\":\"low\"}",
      Task.class
    );
    assertEquals(expected, actual);
  }
}
