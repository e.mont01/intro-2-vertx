create table tasks (
    id int AUTO_INCREMENT PRIMARY KEY,
    description varchar(500) not null,
    priority varchar(5) not null default 'low'
);

